// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

interface BlockPtr {
        function getHead() external view returns (address);
        function getTail() external view returns (address);
}

contract Data {

    mapping(uint16 => uint256) partData;
    mapping(address => bool) authorized;

    modifier isAuthorized(address _address) {
        require(authorized[_address] == true, "Error: Insufficent permission");
        _;
    }

    address public _previousBlock;
    address public _nextBlock;

    constructor(address _previous, address _next, address author, uint256[] memory data) public {
        _previousBlock = _previous;
        _nextBlock = _next;
        authorized[author] = true;
        for (uint16 i = 0; i < data.length; i++) {
            partData[i] = data[i];
        }
    }

    function setPrevious(address _previous) external isAuthorized(msg.sender) {
        _previousBlock = _previous;
    }

    function setNext(address _next) external isAuthorized(msg.sender) {
        _nextBlock = _next;
    }

    function getHead() external view returns (address) {
        BlockPtr blockPtr;
        address _head = Data._previousBlock;
        
        if(_head == 0x0000000000000000000000000000000000000000) {
            return address(this);
        } else {
            while(_head != 0x0000000000000000000000000000000000000000) {
                blockPtr = BlockPtr(_head);
                _head = blockPtr.getHead();
            }
            return _head;
        }
    }

    function getTail() external view returns (address) {
        BlockPtr blockPtr;
        address _tail = Data._previousBlock;
        
        if(_tail == 0x0000000000000000000000000000000000000000) {
            return address(this);
        } else {
            while(_tail != 0x0000000000000000000000000000000000000000) {
                blockPtr = BlockPtr(_tail);
                _tail = blockPtr.getHead();
            }
            return _tail;
        }
    }

    function returnDataParts() external view returns(uint256[] memory) {
        uint256[] memory returnData = new uint256[](100);
        for(uint i = 1; i<=100; i++) {
            returnData[i] = partData[uint16(i)];
        }
        return returnData;
    }
    
}
